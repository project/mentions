<?php

namespace Drupal\mentions\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines Mention Type annotation object.
 *
 * @Annotation
 */
class Mention extends Plugin {
  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The name of the flavor.
   *
   * @ingroup plugin_translatable
   */
  public Translation $name;

}
