<?php

namespace Drupal\mentions\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mentions\MentionsPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MentionsTypeForm.
 *
 * {@inheritdoc}
 *
 * @package Drupal\mentiona\Form
 */
class MentionsTypeForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The mentions plugin manager.
   */
  protected MentionsPluginManager $mentionsManager;

  /**
   * The entity type repository service.
   */
  protected EntityTypeRepositoryInterface $entityTypeRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(MentionsPluginManager $mentions_manager, EntityTypeRepositoryInterface $entity_type_repository) {
    $this->mentionsManager = $mentions_manager;
    $this->entityTypeRepository = $entity_type_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mentions'),
      $container->get('entity_type.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mentions_type_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $plugin_names = $this->mentionsManager->getPluginNames();
    $entity = $this->entity;
    $inputsettings = $entity->getInputSettings();
    $all_entitytypes = \array_keys($this->entityTypeRepository->getEntityTypeLabels());
    $candidate_entitytypes = [];
    foreach ($all_entitytypes as $entity_type) {
      $entitytype_info = $this->entityTypeManager->getDefinition($entity_type);
      $configentityclassname = ContentEntityType::class;
      $entitytype_type = \get_class($entitytype_info);
      if ($entitytype_type == $configentityclassname) {
        $candidate_entitytypes[$entity_type] = $entitytype_info->getLabel()
          ->getUntranslatedString();
        $candidate_entitytypefields[$entity_type][$entitytype_info->getKey('id')] = $entitytype_info->getKey('id');

        if ($entity_type === 'user') {
          $candidate_entitytypefields[$entity_type]['name'] = 'name';
        }
        else {
          $candidate_entitytypefields[$entity_type][$entitytype_info->getKey('label')] = $entitytype_info->getKey('label');
        }
      }
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#description' => $this->t('The human-readable name of this mention type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
      '#default_value' => $this->entity->get('name'),
    ];

    $form['mention_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Mention Type'),
      '#options' => $plugin_names,
      '#default_value' => $this->entity->get('mention_type'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Describe this mention type.'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['input'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Input Settings'),
      '#tree' => TRUE,
    ];
    $input = $this->entity->getInputSettings();
    $form['input']['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#default_value' => !empty($input['prefix']) ? $input['prefix'] : '',
      '#size' => 2,
    ];

    $form['input']['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#default_value' => !empty($input['suffix']) ? $input['suffix'] : '',
      '#size' => 2,
    ];

    $form['input']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $candidate_entitytypes,
      '#default_value' => !empty($input['entity_type']) ? $input['entity_type'] : '',
      '#ajax' => [
        'callback' => [$this, 'changeEntityTypeInForm'],
        'wrapper' => 'edit-input-value-wrapper',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Please wait...'),
        ],
      ],
    ];

    if (!isset($candidate_entitytypefields)) {
      $inputvalue_options = [];
    }
    elseif (!empty($input['entity_type'])) {
      $inputvalue_options = $candidate_entitytypefields[$input['entity_type']];
    }
    else {
      $inputvalue_options = \array_values($candidate_entitytypefields)[0];
    }

    $inputvalue_default_value = \count($inputsettings) == 0 ? 0 : $inputsettings['inputvalue'];

    $form['input']['inputvalue'] = [
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#options' => $inputvalue_options,
      '#default_value' => $inputvalue_default_value,
      '#prefix' => '<div id="edit-input-value-wrapper">',
      '#suffix ' => '</div>',
      '#validated' => 1,
    ];

    $form['output'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Output Settings'),
      '#tree' => TRUE,
    ];
    $output = $this->entity->getOutputSettings();
    $form['output']['outputvalue'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#description' => $this->t('This field supports tokens.'),
      '#default_value' => !empty($output['outputvalue']) ? $output['outputvalue'] : '',
    ];

    $form['output']['renderlink'] = [
      '#type' => 'checkbox',
      '#title' => 'Render as link',
      '#default_value' => !empty($output['renderlink']) ? $output['renderlink'] : '',
    ];

    $form['output']['renderlinktextbox'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link'),
      '#description' => $this->t('This field supports tokens.'),
      '#default_value' => !empty($output['renderlinktextbox']) ? $output['renderlinktextbox'] : '',
      '#states' => [
        'visible' => [
          ':input[name="output[renderlink]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['output']['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => 'all',
        '#show_restricted' => TRUE,
        '#theme_wrappers' => ['form_element'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save Mentions Type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('entity.mentions_type.list');
  }

  /**
   * {@inheritdoc}
   */
  public function changeEntityTypeInForm(array &$form, FormStateInterface $form_state) {
    $entitytype_state = $form_state->getValue(['input', 'entity_type']);
    $entitytype_info = $this->entityTypeManager->getDefinition($entitytype_state);
    $id = $entitytype_info->getKey('id');
    $label = $entitytype_info->getKey('label');
    if ($entitytype_state == 'user') {
      $label = 'name';
    }
    unset($form['input']['inputvalue']['#options']);
    unset($form['input']['inputvalue']['#default_value']);
    $form['input']['inputvalue']['#options'][$id] = $id;
    $form['input']['inputvalue']['#options'][$label] = $label;
    $form['input']['inputvalue']['#default_value'] = $id;
    return $form['input']['inputvalue'];
  }

}
