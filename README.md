# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Record, render and react to specified patterns within content.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/mentions>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/mentions>


## REQUIREMENTS

Drupal core filter.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Add content with @username and it will be rendered as a hyperlink to user
       page.
    3. For more fancy setup, look at Structure->Mentions Type


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
